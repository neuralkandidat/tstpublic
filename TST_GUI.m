function varargout = TST_GUI(varargin)
%TST_GUI M-file for TST_GUI.fig
%      TST_GUI, by itself, creates a new TST_GUI or raises the existing
%      singleton*.
%
%      H = TST_GUI returns the handle to a new TST_GUI or the handle to
%      the existing singleton*.
%
%      TST_GUI('Property','Value',...) creates a new TST_GUI using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to TST_GUI_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      TST_GUI('CALLBACK') and TST_GUI('CALLBACK',hObject,...) call the
%      local function named CALLBACK in TST_GUI.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TST_GUI

% Last Modified by GUIDE v2.5 11-May-2016 14:52:59

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @TST_GUI_OpeningFcn, ...
    'gui_OutputFcn',  @TST_GUI_OutputFcn, ...
    'gui_LayoutFcn',  [], ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before TST_GUI is made visible.
function TST_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Choose default command line output for TST_GUI
set(handles.figure1,'KeyPressFcn',@hotkey);
load('N1-Segmentation/29x29/nets/CleganeBowl/net-epoch-48.mat')
handles.segNet=net;
load('N2-Classification/nets/introducing_hands13/net-epoch-240.mat')
handles.classNet=net;
init;
handles.output = hObject;
handles.cam = webcam;
handles.isRunning = false;
% Update handles structure
guidata(hObject, handles);
% UIWAIT makes TST_GUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);




% --- Outputs from this function are returned to the command line.
function varargout = TST_GUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object creation, after setting all properties.
function imageDisplay_CreateFcn(hObject, eventdata, handles)
% hObject    handle to imageDisplay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate imageDisplay



function imageDisplay_OpeningFcn(hObject, eventdata, handles)
% hObject    handle to imageDisplay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% --- Executes on button press in snapshotButton.
function snapshotButton_Callback(hObject, eventdata, handles)
% hObject    handle to snapshotButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
switch handles.isRunning
    case true
        
        img=snapshot(handles.cam);
        axes(handles.imageDisplay)
        imagesc(flip(img,2));
        handles.isRunning=false;
        
        set(handles.snapshotButton, 'String', 'Start camera');
        set(handles.closebutton, 'String', 'Close');
        
        if handles.evaluate
            set(handles.letter,'String','Evaluating...');
            mask=evaluateImage(handles.segNet,img,0,0,0);
            maskDisp = im2single(imresize(img, size(mask))).*single(repmat(mask,[1,1,3]));
            %mask=mask*255;
            axes(handles.MaskDisplay);
            imagesc(flip(maskDisp,2));
            probs=classifyImageJury(img,mask);
            set(handles.letter,'String',genAnswer(probs,hObject,handles));
            %  handles.mask=mask;
            %  handles.img=img;
            
        end
        guidata(hObject,handles);
    case false
        handles.isRunning=true;
        set(handles.snapshotButton, 'String', 'Take picture');
        set(handles.closebutton, 'String', 'Stop');
        handles.evaluate = true;
        guidata(hObject,handles);
        while handles.isRunning
            img=snapshot(handles.cam);
            axes(handles.imageDisplay);
            imagesc(flip(img,2));
            handles = guidata(hObject);
        end
        
end


% --- Executes when figure1 is resized.
function figure1_SizeChangedFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in closebutton.
function closebutton_Callback(hObject, eventdata, handles)
% hObject    handle to closebutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if handles.isRunning
    handles.evaluate=false;
    snapshotButton_Callback(hObject,eventdata,handles);
else
    clear('handles.webcam');
    guidata(gca,handles);
    close all
end

function hotkey(src, e)
switch e.Key
    case 'space'
        snapshotButton_Callback(src, [], guidata(gca));
    case 'escape'
        closebutton_Callback(src, [], guidata(gca));
        %    case 's'
        %        save_Callback(src, [], guidata(gca));
end



function letter_Callback(hObject, eventdata, handles)
% hObject    handle to letter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of letter as text
%        str2double(get(hObject,'String')) returns contents of letter as a double


% --- Executes during object creation, after setting all properties.
function letter_CreateFcn(hObject, eventdata, handles)
% hObject    handle to letter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function MaskDisplay_CreateFcn(hObject, eventdata, handles)
% hObject    handle to MaskDisplay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate MaskDisplay


% --- Executes on button press in save.
function save_Callback(hObject, eventdata, handles)
% hObject    handle to save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of save
if ~handles.isRunning
    try
        
        [fileName, filePath]=uiputfile({'*.png';'*.jpg'},'Choose a parent folder and name your file');
        if exist(strcat(filePath,'GUI'))~=7
            mkdir(strcat(filePath,'GUI'));
            mkdir(strcat(filePath,'GUI/masks'));
            mkdir(strcat(filePath,'GUI/pics'));
        end
        [path,name,ext]=fileparts(fileName);
        imwrite(handles.mask,strcat(filePath,'/GUI/masks/',strcat(name,'_mask.png')));
        imwrite(handles.img,strcat(filePath,'/GUI/pics/',strcat(name,'.jpg')));
    catch
        
    end
end

function answer=genAnswer(probs, hObject, handles)

[sortedProbs,sortingIndices] = sort(probs,'descend');


answer=[handles.classNet.meta.classes(sortingIndices(1)), ' with ', num2str(sortedProbs(1)*100), '% confidence. ', ...
          handles.classNet.meta.classes(sortingIndices(2)), ' with ', num2str(sortedProbs(2)*100), '% confidence. ', ...
          handles.classNet.meta.classes(sortingIndices(3)), ' with ', num2str(sortedProbs(3)*100), '% confidence.'];



function init
run Lib/vlfeat/toolbox/vl_setup ;
run Lib/matconvnet/matlab/vl_setupnn ;
addpath Lib/matconvnet/examples ;
addpath N1-Segmentation/29x29 ;
addpath N2-Classification/V1.4 ;
addpath Lib/colorspace/colorspace ;
addpath Evaluation;
