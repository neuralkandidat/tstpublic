%
% Evaluates an image.
%
%     function [output] = evaluateImage(net,img_path,stupid,interpolate)
%
%   net:         the net to use
%   img_path:    the path to the image to use
%   stupid:      choose the stupid or smart mode (default: 0)
%   interpolate: use interpolation mode, faster but less accurate (default: 1)
%   verbose:     show system output true/false (default true)
%   threshold:   threshold for which evaluation of pixels (default 0.3)

function [output, t] = evaluateImage(net,img_path,stupid,interpolate,verbose, threshold)
    %init();
    

    if ~exist('net');      error('Net not supplied.'); end
    if ~exist('img_path'); error('Image path not supplied.'); end
    if ~exist('stupid');      stupid = 0; end
    if ~exist('interpolate'); interpolate = 0; end
    if ~exist('verbose'); verbose = true; end
    if ~exist('threshold'); threshold = 0.3; end
    
    
    im_size = 174;
    padding = 14;
    gpu = false;
    cspace='luv';

    if (gpu==true)
        net = vl_simplenn_move(net, 'gpu');
    end
    % Change last layer of net to allow evaluation
    net.layers{end} = [];
    net.layers{end}.type = 'softmax';
    
    tic

    % Read and preprocess image
    if isstr(img_path)
        image=imread(img_path);
    else
        image = img_path;
    end
    
    tic
    image=N1_preprocessImage(image, im_size, padding, cspace);
    t = toc;
    
    if stupid
        output = zeros(im_size,im_size);
        for i=1:im_size
            for j=1:im_size
                output(i,j)=N1_evaluatePixel(net, ...
                    image(i:i+2*padding,j:j+2*padding,:));
            end
        end
        count = im_size^2;
    else 
        [output,count] = smart_sweep(image,[im_size,im_size],16,net,interpolate,threshold);
        output = output>0;
        output=findBiggestIsland(output);
    end
    if verbose
    fprintf(['Done after %.2f seconds\n' ...
             'Evaluated %.1f%% of pixels\n' ...
             'Mask is %.1f%% of image\n'], ...
             toc, 100*count/im_size^2, ...
             100*sum(output(:)>0)./numel(output));
    end
    %imagesc(output);
end

function A = neigh(I,imsize,i)
    [y,x] = ind2sub(imsize, i);
    A = I(y:y+28, x:x+28, :);
end

function r=eval_batch(I,imsize,ii,net,threshold) % {{{1
    pxgroup = cell(length(ii),1);
    
    for i = 1:length(ii)
        pxgroup{i} = neigh(I,imsize,ii(i));
    end
    
    r = zeros(size(ii));
    
    for i = 1:length(ii)
        r(i) = N1_evaluatePixel(net, pxgroup{i});
    end
    r = (r > threshold);
end

function [R,count] = smart_sweep(I,imsize,ssize,net,check_neighbors,threshold) % {{{1
    if nargin <= 4
        check_neighbors = 1;
    end
    
    count = 0;

    % Reshape image to a multiple of ssize
    h = imsize(1);
    w = imsize(2);
    rsize = [h,w];

    % Preallocate the result matrix as nans
    R = nan(h,w);

    %% First pass {{{2
    % Initial search grid
    [X,Y] = meshgrid(1:ssize:h, 1:ssize:w);
    x = X(:); y = Y(:);
    ii = sub2ind(rsize,y,x);
    
    % Search
    r = eval_batch(I,rsize,ii,net,threshold);
    count = count + length(ii);
    R(ii) = r*ssize;
    
    % Remove misses
    x(r==0) = [];
    y(r==0) = [];
    
    %% More passes {{{2
    % Coordinates in hand
    handx = x; handy = y;

    % Halve ssize until the ssize size have been 1
    while ssize > 1
        ssize = ssize / 2;

        x = handx; y = handy;

        while length(x) ~= 0
            x = [x-ssize;  x;        x;  x;        x+ssize];
            y = [y;        y-ssize;  y;  y+ssize;  y];
            
            % Check bounds
            jj = x<=0 | y<=0 | x>w | y>h;
            x(jj) = []; y(jj) = [];

            % Get unique indices
            ii = unique(sub2ind(rsize,y,x));
            
            % Remove already handled points
            ii(~isnan(R(ii))) = [];
            
            % Hacks
            if check_neighbors
                [Y,X] = ind2sub(size(R),ii);
                for i = 1:length(ii)
                    ix = X(i);
                    iy = Y(i);
                    if ix <= ssize || ix > (length(R)-ssize); continue; end
                    if iy <= ssize || iy > (length(R)-ssize); continue; end

                    % If two opposite (say NW/SE or N/S) sample points
                    % are set, then this sample point is likely also a hit.
                    if (((R(iy-ssize, ix-ssize) > 0) && (R(iy+ssize, ix+ssize) > 0)) + ...
                        ((R(iy-ssize, ix+ssize) > 0) && (R(iy+ssize, ix-ssize) > 0)) + ...
                        ((R(iy, ix-ssize) > 0) && (R(iy, ix+ssize) > 0)) + ...
                        ((R(iy-ssize, ix) > 0) && (R(iy+ssize, ix) > 0))) >= 1
                       
                        R(ii(i)) = ssize;
                    end
                end
            end
            
            % Calculate a batch (split done/evaluate)
            dii = ii(~isnan(R(ii)));
            eii = ii(isnan(R(ii)));
            r = eval_batch(I,rsize,eii,net,threshold);
            count = count + length(eii);
            R(eii) = r*ssize;
            eii(r~=1) = []; % Remove misses

            % Add new points to target list
            [y,x] = ind2sub(rsize,[eii;dii]);
            handx = [handx; x];
            handy = [handy; y];
        end
    end % }}}2

    R(isnan(R)) = 0;
end

function init()
    addpath ../N1-Segmentation/29x29
    addpath ../Lib/colorspace/colorspace

    run ../Lib/matconvnet/matlab/vl_setupnn
end
