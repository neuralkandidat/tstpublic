function [probability] = classifyImage(image, mask, net)

if ~exist('net');       error('Net not supplied.'); end
if ~exist('image');     error('Image not supplied.'); end
if ~exist('mask');      error('Mask not supplied.'); end

net.layers{end} = [];
net.layers{end}.type = 'softmax';

imSize=64; %static, net takes 64x64x2 (grayscale im + mask) as input

image = N2_preprocessImage(image, mask, imSize);

[res] = vl_simplenn(net,image,[],[],'mode','test');

probability = squeeze(res(end).x(:,:,:));

end