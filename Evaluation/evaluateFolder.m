function evaluateFolder(sourcepath, destpath, net)
size = 174;
padding = 14;
gpu = false;
cspace='luv';

source_struct = dir(strcat(sourcepath,'*.jpg'));

sorted_names=sortrows({source_struct.name});
nrOfImages=length(sorted_names);


for i = 1:nrOfImages
    letter = sorted_names{i}(4);
    image = evaluateImage(net, strcat(sourcepath,sorted_names{i}),0,0);
    display(strcat('Done evaluating:', sorted_names{i}));
    [path,file]=fileparts(sorted_names{i});
    imwrite(image,strcat(destpath, file,'_mask.png') );
    
end