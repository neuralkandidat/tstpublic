function [probability,t] = classifyImageJury(image, mask)

if ~exist('image');     error('Image not supplied.'); end
if ~exist('mask');      error('Mask not supplied.'); end

load('N2-Classification/nets/prutt/net-epoch-95.mat');
net.layers{end} = [];
net.layers{end}.type = 'softmax';
net1 = net;

load('N2-Classification/nets/introducing_hands13/net-epoch-470.mat');
net.layers{end} = [];
net.layers{end}.type = 'softmax';
net2 = net;

load('N2-Classification/nets/JackeFille2/net-epoch-1000.mat');
net.layers{end} = [];
net.layers{end}.type = 'softmax';
net3 = net;


net.layers{end} = [];
net.layers{end}.type = 'softmax';
tic
[image1, image2, image3] = N2_preprocessImageJury(image, mask);
t=toc;
res1 = vl_simplenn(net1,image3,[],[],'mode','test');
res2 = vl_simplenn(net2,image2,[],[],'mode','test');
res3 = vl_simplenn(net3,image3,[],[],'mode','test');

r1 = squeeze(res1(end).x(:,:,:));
r2 = squeeze(res2(end).x(:,:,:));
r3 = squeeze(res3(end).x(:,:,:));

probability = (points(r1) + points(r2) + points(r3))./(3*(2.^(numel(r1)-1)-1));

end

function p = points(x)
  w = 2.^(0:numel(x)-1)-1;  % Weights
  [~,ind] = sort(x);        % Get sorting index
  % Distribute points
  p(ind) = w;
  p = p';
end