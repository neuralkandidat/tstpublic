%Removes noise from mask, outputs mask with only hand.
%P - remove all cc:s that have less than P pixels.
function mask = removeIslands(mask,P)

%remove cc:s that are smaller than P pixels
mask=bwareaopen(mask,P);

%select the two largest cc:s, hopefully hand and face
mask=bwareafilt(mask,2,'largest');
BW=bwconncomp(mask);
stats=regionprops(BW);


if BW.NumObjects==0
    return;
end

index = 0;
%select index of the leftmost cc
try
    if stats(1).BoundingBox(1)>stats(2).BoundingBox(1)
        index = 1;
    else
        index = 2;
    end
    mask(BW.PixelIdxList{index})=0;

catch
    
end

