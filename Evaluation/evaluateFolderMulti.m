function evaluateFolderMulti(srcPath,destPath,net)
mkdir(destPath);

source_struct = dir(srcPath);
source_struct = removeIllegal(source_struct);

sorted_folders=sortrows({source_struct.name});
nrOfFolders=length(sorted_folders);

for i = 1:nrOfFolders
    mkdir(strcat(destPath,sorted_folders{i}));
    evaluateFolder(strcat(srcPath,sorted_folders{i},'/'),strcat(destPath,sorted_folders{i},'/'),net);
end
end

function A=removeIllegal(B)
A=B;
for i=1:length(B)
    if B(length(B)-i+1).name(1) == '.'
        A(length(B)-i+1)=[];
    end
end
end
