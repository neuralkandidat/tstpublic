image = imread('J_A01.jpg');
mask = imread('J_A01.jpg_mask.png');
%%
image = imread('J_C01.jpg');
mask = imread('J_C01.jpg_mask.png');
%%
image = imread('J_I01.jpg');
mask = imread('J_I01.jpg_mask.png');
%%
image = imread('J_L01.jpg');
mask = imread('J_L01.jpg_mask.png');
%%
image = imread('S_A01.jpg');
mask = imread('S_A01.jpg_mask.png');
%%
image = imread('S_C01.jpg');
mask = imread('S_C01.jpg_mask.png');
%%
image = imread('S_I01.jpg');
mask = imread('S_I01.jpg_mask.png');
%%
image = fliplr(imread('003C_F.jpg'));
mask = fliplr(imread('003C_F_mask.png'));
%%

ppimage = N2_preprocessImage(image,mask,64)


load('net-epoch-8.mat')
net.layers{end}=[];
net.layers{end}.type = 'softmax';

res = vl_simplenn(net, ppimage);

res(12).x