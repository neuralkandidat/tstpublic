function [im, labels] =  N2_getBatch(imdb, batch)
%assumes that all images in imdb are of the same size
im=single(zeros(64,64,4,length(batch)));
labels=single(zeros(1, length(batch)));

for i=1:length(batch)
    im(:,:,:,i)=imdb.images.data{batch(i)};
    labels(1,i)=imdb.images.gt(batch(i));
end

end
