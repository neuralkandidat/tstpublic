%Function creates an IMDB structure

%Preconditions:
%
%Directory:     Directories with images must end with _masks
%               and start with one letter labeling the class, i.e 'A','B'
%               etc
%
%Image files:   Files for RGB images must be 11 characters long and have
%               the extension '.jpg'
%               Example: '001I-01-A-C.jpg'
%
%               Files for masks must have the same characters with the
%               addition of '_mask' and have the extension '.png'
%               Example: '001I-01-A-C_mask.png'

% masterdir master directory, ex: 'neural-kandidat/images-masked'
function imdb = N2_imdbmaker(parentDir, name)
display('Creating IMDB...');

%Create IMDB structure
imdb={};

% data contains data for mask projected onto image
%
% set contains integers {1,2,3} which determines if image should
% be used for training, validatation or testing of network
%
% size contains image size for each image

imdb.images.data={};
imdb.images.set=[];
imdb.images.id=[];
imdb.images.gt=[];

imdb.meta.mean={};
imdb.meta.sets={'train','val','test'};
imdb.meta.classes = [];
% id is a vector of 1:amount of classes
%
% label contains names of the classifications
%
% images cumulative sum vector for how how many images of each class
% there are, i.e. images=[4, 9] tells us that images 1 to 4 are of class 1,
% and images 5-9 are of class 2.
%imdb.classes.id=[];
%imdb.classes.label={};
%imdb.classes.images=[];

% sets is required by matconvnet and refers to the integers in
% imdb.images.set, i.e. 1 for training, 2 for validation, 3 for testing

% Check OS, adjust accordingly
if ispc
    separator = '[^;]*';
else
    separator='[^:]*';
end


maskdirs = dir(strcat(parentDir,'/masks'));
isub = [maskdirs(:).isdir]; %# returns logical vector
maskdirs = {maskdirs(isub).name}';
maskdirs(ismember(maskdirs,{'.','..'})) = [];

imdirs = dir(strcat(parentDir,'/pics'));
isub = [imdirs(:).isdir]; %# returns logical vector
imdirs = {imdirs(isub).name}';
imdirs(ismember(imdirs,{'.','..'})) = [];

for i=1:length(maskdirs) %omits i=1 since this refers to parentDir itself
    maskdir=char(strcat(parentDir,'/masks/',maskdirs(i)));
    imdir=char(strcat(parentDir,'/pics/',imdirs(i)));
    maskpaths=dir(maskdir);
    impaths=dir(imdir);
    
    % Remove elements of paths that start with '.', e.g hidden files.
    maskpaths=removeIllegal(maskpaths);
    impaths=removeIllegal(impaths);
    
    
    
    
    for j=1:length(impaths)
        % If the mask name does not match the image name, an error will
        % be thrown. Name of current image and mask included in message
        %if checkMatch(masks(i).name,images(i).name)
        letter = maskpaths(j).name(4);
        if ~(strcmp(letter,'J')||(strcmp(letter,'Z')))
            disp(maskpaths(j).name)
            imdb=addToImdb(strcat(imdir,'/',impaths(j).name),strcat(maskdir,'/',maskpaths(j).name),imdb, letter);
        end
        %else
        % error(strcat('Image does not match mask. Image: ',imdir,'/', images(i).name,', mask: ',maskdir,'/',masks(i).name));
        %end
        
    end
end
%Fix index of classes

% number of classes
imdb.meta.numClasses = length(unique(cell2mat(imdb.images.gt)));

imdb.images.gt = cell2mat(imdb.images.gt);

for i=1:imdb.meta.numClasses
    j=i;
    while (isempty(find(imdb.images.gt==j)))
        j=j+1;
    end
    imdb.images.gt(imdb.images.gt==j)=i;
end

% Create a imdb.images.set, parameters are the amounts
% (0 to 1) of elements in train, val and test. Random order.
imdb=createSet(0.8,0.2,0,imdb);
imdb.images.id=1:numel(imdb.images.gt);

% Create folder imdbs if it does not exist
if exist('Data/imdbs')~=7
    mkdir('Data/imdbs')
end
save(strcat('Data/imdbs/', name),'imdb','-v7.3')

end

function imdb = addToImdb(imagepath, maskpath, imdb, gt)
image = imread(imagepath);
mask = imread(maskpath);

mask = mask(:,:,1);

%should not be needed
%if isempty(find(mask(:), 1))
%    return;
%end

data=N2_preprocessImage(image,mask,64);

imdb.images.data{end+1}=data;
gt = char(gt);
imdb.images.gt{end+1}=gt-64;
if isempty(find(imdb.meta.classes==gt))
    imdb.meta.classes = [imdb.meta.classes gt];
end

end


function A=removeIllegal(B)
A=B;
for i=1:length(B)
    if B(length(B)-i+1).name(1) == '.'
        A(length(B)-i+1)=[];
    end
end
end

function match=checkMatch(image,mask)
image=strsplit(image,'.');
mask=strsplit(mask,'.');

if (strcat(image{1},'_mask') == mask{1})
    match=1;
else
    match=0;
end
end

function imdb=createSet(trainAmount,valAmount,testAmount,imdb);
train=ones(1,round(trainAmount*numel(imdb.images.gt)));
val=2*ones(1,round(valAmount*numel(imdb.images.gt)));
test=3*ones(1,round(testAmount*numel(imdb.images.gt)));

imdb.images.set=horzcat(train,val,test);
imdb.images.set=imdb.images.set(randperm(length(imdb.images.set)));
end

