function net=N2_train(imdb, numEpochs, expDir)
clear net;
net=N2_initializeCNN_dropout(imdb.meta.numClasses);
net.meta.classes = imdb.meta.classes;
trainOpts.batchSize = 10;
trainOpts.numEpochs = numEpochs ;
trainOpts.continue = true ;
trainOpts.learningRate = 0.00001 ;
opts.train.errorFunction = 'label';
trainOpts.expDir = strcat('N2-Classification/V1.4/nets/',expDir) ;

trainOpts = vl_argparse(trainOpts, {});
[net,info] = cnn_train(net, imdb, @N2_getBatch, trainOpts) ;
end