function image = N2_preprocessImage(image, mask, imSize)


image = rgb2gray(image);
mask = imresize(mask, size(image),'nearest');

%Klippa ut kvadratiska områden
[image, mask] = padAndCrop(image, mask);

image=im2single(image);
mask=im2single(mask);
%image=normalize(image);

image = imresize(image, [imSize imSize]);
mask = imresize(mask, [imSize imSize],'nearest');

image = repmat(image,1,1,2);

mask = logical(mask).*sum(sum(image(:,:,1)))/numel(image(:,:,1));

image(:,:,2) = mask;
image=image*100;
end

function image=normalize(image)
image=(image-mean(mean(image)))./std(std(image));
end
