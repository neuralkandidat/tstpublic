function [croppedImage, croppedMask] = padAndCrop(image,mask)
maxW=size(image,2);
maxH=size(image,1);

x=find(sum(mask));
y=find(sum(mask'));

w=length(x);%abs(x(1)-x(end));
h=length(y); %abs(y(1)-y(end));

%positive if y>x, 0 if equal
sizeDiff=h-w;
padLength = ceil(abs(sizeDiff/2));
cropLength = max(w,h);

cropx=x(1);
cropy=y(1);

if sizeDiff < 0 % w>h
    
    if y(end)+padLength>maxH 
        cropy = maxH-cropLength;
        
    elseif cropy-padLength<0
        cropy=1;
    else
        cropy = cropy-padLength;
    end
    
elseif sizeDiff > 0 % w<h

    if x(end)+padLength>maxW 
        cropx = maxW-cropLength;
        
    elseif cropx-padLength<0
        cropx=1;
    else
        cropx = cropx-padLength;
    end
end

croppedImage=image(max(cropy,1):min(cropy+cropLength,maxH),max(cropx,1):min(cropx+cropLength,maxW),:);
croppedMask=mask(max(1,cropy):min(cropy+cropLength,maxH),max(1,cropx):min(cropx+cropLength,maxW));
end
