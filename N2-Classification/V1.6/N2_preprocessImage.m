function output = N2_preprocessImage(image, mask, imSize)


image = colorspace('rgb->luv',image);
mask = imresize(mask, size(image(:,:,1)),'nearest');

%Klippa ut kvadratiska omr?den
[image, mask] = padAndCrop(image, mask);

image=im2single(image);
mask=im2single(mask);
image=normalize(image);

image = imresize(image, [imSize imSize]);
mask = imresize(mask, [imSize imSize],'nearest');

mask = logical(mask).*sum(sum(image(:,:,1)))/numel(image(:,:,1));

output = cat(3,image, mask);
%output = output * 255;
end

function im_mean=calculateImageMean(image);
    im_mean=mean(mean(image(:,:)));
end 

function im_std=calculateImageStd(image);
        im_std=std(std(image(:,:)));
end

function image=normalize(image);
    for i=1:3
        image(:,:,i)=(image(:,:,i)-calculateImageMean(image(:,:,i)))./calculateImageStd(image(:,:,i));
    end
end