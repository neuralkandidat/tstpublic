function net=N2_train(imdb, numEpochs, expDir)
clear net;
net=N2_initializeCNN_dropout(imdb.meta.numClasses);
net.meta.classes = imdb.meta.classes;
trainOpts.batchSize = 24;
trainOpts.numEpochs = numEpochs ;
trainOpts.continue = true;
trainOpts.learningRate = 0.0001 ;
opts.train.errorFunction = 'label';
trainOpts.expDir = strcat('N2-Classification/nets/',expDir) ;

trainOpts = vl_argparse(trainOpts, {});
[net,info] = cnn_train(net, imdb, @N2_getBatch, trainOpts) ;
end