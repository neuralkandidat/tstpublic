run Lib/vlfeat/toolbox/vl_setup ;
run Lib/matconvnet/matlab/vl_setupnn ;
addpath Lib/matconvnet/examples ;
addpath N1-Segmentation/29x29 ;
addpath Lib/colorspace/colorspace ;
addpath Utils/Tools ;
addpath Evaluation
