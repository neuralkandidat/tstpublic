%evaluates images with different nets and plots results

function dataGeneratorBatch2(setPath, setNumber)


if setNumber < 10
    setNumber=strcat('0',num2str(setNumber));
else
    setNumber=strcat(setNumber);
end

dir_struct = dir(strcat(setPath,'*.jpg'));

sorted_names=sortrows({dir_struct.name}');
nrOfFiles=size(sorted_names,1);

images = uint8(zeros(720,1080,3,nrOfFiles));

for i = 1:nrOfFiles
    img = imread(sorted_names{i});
    if size(img,2)~=1080
        img = imresize(img, [720, 1080]);
    end
    images(:,:,:,i) = img;
end
display('Loaded all images')

masks = evaluateBatch(images, nrOfFiles);

for i = 1:nrOfFiles
    savePictures(sorted_names{i}(1:8), images(:,:,:,i), masks(:,:,i));
end

%chooseAndSave(images, masks, nrOfFiles, setNumber);
end


function masks = evaluateBatch(images,nrOfFiles)
masks = zeros(174, 174, nrOfFiles);

%18 AMIGOS
load('N1-Segmentation/29x29/nets/S_CleganeBowl/net-epoch-11.mat');
for i=1:nrOfFiles
    masks(:,:,i) = evaluateImage(net,images(:,:,:,i),0,0, false);
end
display('Clegane Bowl confirmed')
end

function savePictures(name, image, masks)
if(exist('Data/generated/pics/')~=7)
    mkdir('Data/generated/pics')
end

%imwrite(image, strcat('Data/generated/pics/',name,'.jpg'));



if(exist('Data/generated/S_CleganeBowl/')~=7)
    mkdir('Data/generated/S_CleganeBowl')
end

imwrite(masks, strcat('Data/Generated/S_CleganeBowl/',name,'_mask.png'));
end

function name = getName(setNumber, letter)
imList=struct2cell(dir(strcat('Data/Generated/pics')));
imList=imList(1,:);

imListSet=strfind(imList,['S',setNumber]);
imListLetter=strfind(imList,letter);
j=0;
inds = 0;
for i=1:length(imListLetter)
    if imListLetter{i}==4
        if imListSet{i}==6
            j=j+1;
            inds(j)=i;
        end
    end
end
%
if inds == 0
    name = strcat('001',letter,'_S',setNumber);
else
    inds(inds==0)=[];
    
    numList=char(imList(inds));
    numList=numList(:,1:3);
    numList=str2num(numList);
    
    num=max(numList)+1;
    if num<10
        numstr=strcat('00',num2str(num));
    elseif num<100
        numstr=strcat('0',num2str(num));
    else
        numstr=strcat(num);
    end
    
    name=strcat(numstr,letter,'_S',setNumber);
end
end
