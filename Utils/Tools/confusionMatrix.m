%cRate     - fraction of samples misclassified
%C         - confusion matrix
%indices   - indices of samples
%per       - false positives/negatives, true positives/negatives

%parentdir - directory with pic-folder and mask-folder

%%%%%OBS: ROW 14&15 ENTER WANTED PATH

function [cRate C indices per] = confusionMatrix(parentDir)
%puts all image and mask names in structs

maskdir = strcat(parentDir,'S_CleganeBowl/');
masks = dir(strcat(parentDir,'S_CleganeBowl/','*.png'));

imdir = strcat(parentDir,'pics/');
images = dir(strcat(parentDir,'pics/','*.jpg'));

numIms=length(images);
numClasses=24;

%matrices for confusion-function
truthMatrix=zeros(numClasses,numIms);
outputMatrix=zeros(numClasses,numIms);


for i=1:numIms
    imName=images(i).name;
    maskName=masks(i).name;
    
    letter=imName(4);
    im=imread(strcat(imdir,imName));
    mask=imread(strcat(maskdir,maskName));
    
    %convert to ascii value and subtract 64 to get matrix index
    index=uint8(letter)-64;
    
    %adjust for exclusion of J
    if index>=10
        index=index-1;
    end
    
    truthMatrix(index,i)=1;
    outputMatrix(:,i)=classifyImage(im,mask,classNet);
    
end

%calculate confusion matrix with BIF
[cRate C indices per]=confusion(truthMatrix,outputMatrix);

end