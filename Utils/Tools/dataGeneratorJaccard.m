%evaluates images with different nets and plots results

function dataGeneratorJaccard(setPath)

dir_struct = dir(strcat(setPath,'*.jpg'));

sorted_names=sortrows({dir_struct.name}');
nrOfFiles=size(sorted_names,1);

images = uint8(zeros(720,1080,3,nrOfFiles));

for i = 1:nrOfFiles
    img = imread(sorted_names{i});
    if size(img,2)~=1080
        img = imresize(img, [720, 1080]);
    end
    images(:,:,:,i) = img;
end
display('Loaded all images')

masks = evaluateBatch(images, nrOfFiles);

for i = 1:nrOfFiles
    savePictures(sorted_names{i}(1:8), images(:,:,:,i), masks(:,:,:,i));
end

end


function masks = evaluateBatch(images,nrOfFiles)
masks = zeros(174, 174, 6, nrOfFiles);

%18 AMIGOS
load('N1-Segmentation/29x29/nets/18amigos_S/net-epoch-33.mat');
for i=1:nrOfFiles
    masks(:,:,1,i) = evaluateImage(net,images(:,:,:,i),0,0, false,0.3);
end
display('Evaluated 18 Amigos')

%2008 IMS
load('N1-Segmentation/29x29/nets/2008ims/net-epoch-22.mat');
for i=1:nrOfFiles
    masks(:,:,2,i) = evaluateImage(net,images(:,:,:,i),0,0, false,0.3);
end
display('Evaluated 2008 IMS')

%JON LIVES
load('N1-Segmentation/29x29/nets/JonLives_S/net-epoch-15.mat');
for i=1:nrOfFiles
    masks(:,:,3,i) = evaluateImage(net,images(:,:,:,i),0,0, false,0.3);
end
display('Evaluated Jon Lives')

%EaterOfWorlds
load('N1-Segmentation/29x29/nets/S_EaterOfWorlds/net-epoch-14.mat');
for i=1:nrOfFiles
    masks(:,:,4,i) = evaluateImage(net,images(:,:,:,i),0,0, false,0.3);
end
display('Evaluated Eater of Worlds')

%EaterOfWorlds dropout
load('N1-Segmentation/29x29/nets/S_EaterOfWorlds_dropout/net-epoch-14.mat');
for i=1:nrOfFiles
    masks(:,:,5,i) = evaluateImage(net,images(:,:,:,i),0,0, false,0.3);
end
display('Evaluated Eater of Worlds dropout')

load('N1-Segmentation/29x29/nets/S_CleganeBowl/net-epoch-21.mat');
for i=1:nrOfFiles
    masks(:,:,6,i) = evaluateImage(net,images(:,:,:,i),0,0, false,0.3);
end
display('Clegane Bowl confirmed')
end

function savePictures(name, image, masks)
if(exist('Data/Jaccard/pics/')~=7)
    mkdir('Data/Jaccard/pics')
end

imwrite(image, strcat('Data/Jaccard/pics/',name,'.jpg'));

if(exist('Data/Jaccard/S_CleganeBowl/')~=7)
    mkdir('Data/Jaccard/S_CleganeBowl')
end

if(exist('Data/Jaccard/pics/')~=7)
    mkdir('Data/Jaccard/pics')
end

imwrite(image, strcat('Data/Jaccard/pics/',name,'.jpg'));

if(exist('Data/Jaccard/18Amigos/')~=7)
    mkdir('Data/Jaccard/18Amigos')
end

if(exist('Data/Jaccard/2008ims/')~=7)
    mkdir('Data/Jaccard/2008ims')
end

if(exist('Data/Jaccard/JonLives/')~=7)
    mkdir('Data/Jaccard/JonLives')
end


if(exist('Data/Jaccard/S_EaterOfWorlds/')~=7)
    mkdir('Data/Jaccard/S_EaterOfWorlds')
end


if(exist('Data/Jaccard/S_EaterOfWorlds_dropout/')~=7)
    mkdir('Data/Jaccard/S_EaterOfWorlds_dropout')
end

imwrite(masks(:,:,1), strcat('Data/Jaccard/18Amigos/',name,'.png'));
imwrite(masks(:,:,2), strcat('Data/Jaccard/2008ims/',name,'.png'));
imwrite(masks(:,:,3), strcat('Data/Jaccard/JonLives/',name,'.png'));
imwrite(masks(:,:,4), strcat('Data/Jaccard/S_EaterOfWorlds/',name,'.png'));
imwrite(masks(:,:,5), strcat('Data/Jaccard/S_EaterOfWorlds_dropout/',name,'.png'));
imwrite(masks(:,:,6), strcat('Data/Jaccard/S_CleganeBowl/',name,'.png'));
end

function name = getName(setNumber, letter)
imList=struct2cell(dir(strcat('Data/Jaccard/pics')));
imList=imList(1,:);

imListSet=strfind(imList,['S',setNumber]);
imListLetter=strfind(imList,letter);
j=0;
inds = 0;
for i=1:length(imListLetter)
    if imListLetter{i}==4
        if imListSet{i}==6
            j=j+1;
            inds(j)=i;
        end
    end
end
%
if inds == 0
    name = strcat('001',letter,'_S',setNumber);
else
    inds(inds==0)=[];
    
    numList=char(imList(inds));
    numList=numList(:,1:3);
    numList=str2num(numList);
    
    num=max(numList)+1;
    if num<10
        numstr=strcat('00',num2str(num));
    elseif num<100
        numstr=strcat('0',num2str(num));
    else
        numstr=strcat(num);
    end
    
    name=strcat(numstr,letter,'_S',setNumber);
end
end
