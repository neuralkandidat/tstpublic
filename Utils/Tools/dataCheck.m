function frequency = dataCheck(parentDir)

maskdir = strcat(parentDir,'masks/');
masks = dir(strcat(parentDir,'masks/','*.png'));

imdir = strcat(parentDir,'pics/');
images = dir(strcat(parentDir,'pics/','*.jpg'));

if length(images)~=length(masks)
    error 'number of masks do not match number of pics');
end

numPics=length(masks);

%lists the number of objects of each class
frequency=zeros(24,1);

for i=1:numPics
    
    picName=images(i).name;
    maskName=masks(i).name;
    
    if ~strcmp(picName(1:8),maskName(1:8))
        disp('lol')
    end
    
    letter=picName(4);
    %convert to number
    letter=uint8(letter)-64;
    
    %account for exclusion of J
    if letter>=10
        letter=letter-1;
    end
    
    frequency(letter)=frequency(letter)+1;
end