%evaluates images with different nets and plots results

function dataGeneratorMass(setPath, setNumber)

if setNumber < 10
    setNumber=strcat('0',num2str(setNumber));
else
    setNumber=num2str(setNumber);
end

dir_struct = dir(strcat(setPath,'*.jpg'));

sorted_names=sortrows({dir_struct.name}');
nrOfFiles=size(sorted_names,1);

images = uint8(zeros(720,1080,3,nrOfFiles));

for i = 1:nrOfFiles
    images(:,:,:,i) = imread(sorted_names{i});
end


masks = evaluateBatch(images, nrOfFiles);

chooseAndSave(images, masks, nrOfFiles, setNumber);
end


function masks = evaluateBatch(images,nrOfFiles)
masks = zeros(174, 174, nrOfFiles);

%ladda r�tt n�t
load('N1-Segmentation/29x29/nets/S_CleganeBowl/net-epoch-21.mat');
for i=1:nrOfFiles
    masks(:,:,i) = evaluateImage(net,images(:,:,:,i),0,0, false, 0.3);
end
display('Evaluated all images')
end

function chooseAndSave(images, masks, nrOfFiles, setNumber)

for i=1:nrOfFiles
    figure1 = figure;
    image = images(:,:,:,i);
    set(figure1,'KeyPressFcn', @(src,eventdata)hotkeys(src,eventdata,image, masks(:,:,i), setNumber));
    subplot(2,1,1);
    imagesc(masks(:,:,i));
    title('Mask');
    
    subplot(2,1,2);
    imagesc(image);
    title('Image');
    
    while(waitforbuttonpress==0)
        
    end
    
    close all
end


end

function hotkeys(src, e, image, masks, setNumber)
code = char(e.Key)-0;

if strcmp(e.Key,'escape')
    return;
elseif (code >=97) && (code <=122)
    display(strcat('Saving as character: ', upper(char(code))));
    name = getName(setNumber, upper(char(code)));
    savePictures(name,image, masks);
end

end

function savePictures(name, image, masks)
if(exist('Data/MassGenerated/pics/')~=7)
    mkdir('Data/MassGenerated/pics')
end

imwrite(image, strcat('Data/MassGenerated/pics/',name,'.jpg'));

if(exist('Data/MassGenerated/masks/')~=7)
    mkdir('Data/MassGenerated/masks')
end

imwrite(masks, strcat('Data/MassGenerated/masks/',name,'_mask.png'));
end

function name = getName(setNumber, letter)
imList=struct2cell(dir(strcat('Data/MassGenerated/pics')));
imList=imList(1,:);

imListSet=strfind(imList,['S',setNumber]);
imListLetter=strfind(imList,letter);
j=0;
inds = 0;
for i=1:length(imListLetter)
    if max(imListLetter{i}==4)==1
        if imListSet{i}==6
            j=j+1;
            inds(j)=i;
        end
    end
end
%
if inds == 0
    name = strcat('001',letter,'_S',setNumber);
else
    inds(inds==0)=[];
    
    numList=char(imList(inds));
    numList=numList(:,1:3);
    numList=str2num(numList);
    
    num=max(numList)+1;
    if num<10
        numstr=strcat('00',num2str(num));
    elseif num<100
        numstr=strcat('0',num2str(num));
    else
        numstr=num2str(num);
    end
    
    name=strcat(numstr,letter,'_S',setNumber)
end
end
