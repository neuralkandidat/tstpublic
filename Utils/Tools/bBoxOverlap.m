function ratio = bBoxOverlap(A,B)
%find indices of non-zero elements in both matrices
A=find(A);
B=find(B);

%calculate ratio as intersection of indices divided by union of indices
ratio=length(intersect(A,B))/length(union(A,B));
end