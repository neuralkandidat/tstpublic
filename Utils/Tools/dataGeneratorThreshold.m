%evaluates images with different nets and plots results

function dataGeneratorThreshold(setPath)

dir_struct = dir(strcat(setPath,'*.jpg'));

sorted_names=sortrows({dir_struct.name}');
nrOfFiles=size(sorted_names,1);

images = uint8(zeros(720,1080,3,nrOfFiles));

for i = 1:nrOfFiles
    img = imread(sorted_names{i});
    if size(img,2)~=1080
        img = imresize(img, [720, 1080]);
    end
    images(:,:,:,i) = img;
end
display('Loaded all images')

masks = evaluateBatch(images, nrOfFiles);

for i = 1:nrOfFiles
    savePictures(sorted_names{i}, images(:,:,:,i), masks(:,:,:,i));
end

display('Finished')
end


function masks = evaluateBatch(images,nrOfFiles)
masks = zeros(174, 174, 9, nrOfFiles);

load('N1-Segmentation/29x29/nets/S_CleganeBowl/net-epoch-11.mat');

for i=1:nrOfFiles
    masks(:,:,1,i) = evaluateImage(net,images(:,:,:,i),0,0, false,0.1);
end
display('Clegane Bowl 0.1 confirmed')

for i=1:nrOfFiles
    masks(:,:,2,i) = evaluateImage(net,images(:,:,:,i),0,0, false,0.2);
end
display('Clegane Bowl 0.2 confirmed')

for i=1:nrOfFiles
    masks(:,:,3,i) = evaluateImage(net,images(:,:,:,i),0,0, false,0.3);
end
display('Clegane Bowl 0.3 confirmed')

for i=1:nrOfFiles
    masks(:,:,4,i) = evaluateImage(net,images(:,:,:,i),0,0, false,0.4);
end
display('Clegane Bowl 0.4 confirmed')

for i=1:nrOfFiles
    masks(:,:,5,i) = evaluateImage(net,images(:,:,:,i),0,0, false,0.5);
end
display('Clegane Bowl 0.5 confirmed')

for i=1:nrOfFiles
    masks(:,:,6,i) = evaluateImage(net,images(:,:,:,i),0,0, false,0.6);
end
display('Clegane Bowl 0.6 confirmed')

for i=1:nrOfFiles
    masks(:,:,7,i) = evaluateImage(net,images(:,:,:,i),0,0, false,0.7);
end
display('Clegane Bowl 0.7 confirmed')

for i=1:nrOfFiles
    masks(:,:,8,i) = evaluateImage(net,images(:,:,:,i),0,0, false,0.8);
end
display('Clegane Bowl 0.8 confirmed')

for i=1:nrOfFiles
    masks(:,:,9,i) = evaluateImage(net,images(:,:,:,i),0,0, false,0.9);
end
display('Clegane Bowl 0.9 confirmed')
end

function savePictures(name, image, masks)
if(exist('Data/CleganeThresh/pics/')~=7)
    mkdir('Data/CleganeThresh/pics')
end

imwrite(image, strcat('Data/CleganeThresh/pics/',name,'.jpg'));


if(exist('Data/CleganeThresh/S_CleganeBowl1/')~=7)
    mkdir('Data/CleganeThresh/S_CleganeBowl1')
end

if(exist('Data/CleganeThresh/S_CleganeBowl2/')~=7)
    mkdir('Data/CleganeThresh/S_CleganeBowl2')
end

if(exist('Data/CleganeThresh/S_CleganeBowl3/')~=7)
    mkdir('Data/CleganeThresh/S_CleganeBowl3')
end

if(exist('Data/CleganeThresh/S_CleganeBowl4/')~=7)
    mkdir('Data/CleganeThresh/S_CleganeBowl4')
end

if(exist('Data/CleganeThresh/S_CleganeBowl5/')~=7)
    mkdir('Data/CleganeThresh/S_CleganeBowl5')
end

if(exist('Data/CleganeThresh/S_CleganeBowl6/')~=7)
    mkdir('Data/CleganeThresh/S_CleganeBowl6')
end

if(exist('Data/CleganeThresh/S_CleganeBowl7/')~=7)
    mkdir('Data/CleganeThresh/S_CleganeBowl7')
end

if(exist('Data/CleganeThresh/S_CleganeBowl8/')~=7)
    mkdir('Data/CleganeThresh/S_CleganeBowl8')
end

if(exist('Data/CleganeThresh/S_CleganeBowl9/')~=7)
    mkdir('Data/CleganeThresh/S_CleganeBowl9')
end

imwrite(masks(:,:,1), strcat('Data/CleganeThresh/S_CleganeBowl1/',name,'.png'));
imwrite(masks(:,:,2), strcat('Data/CleganeThresh/S_CleganeBowl2/',name,'.png'));
imwrite(masks(:,:,3), strcat('Data/CleganeThresh/S_CleganeBowl3/',name,'.png'));
imwrite(masks(:,:,4), strcat('Data/CleganeThresh/S_CleganeBowl4/',name,'.png'));
imwrite(masks(:,:,5), strcat('Data/CleganeThresh/S_CleganeBowl5/',name,'.png'));
imwrite(masks(:,:,6), strcat('Data/CleganeThresh/S_CleganeBowl6/',name,'.png'));
imwrite(masks(:,:,7), strcat('Data/CleganeThresh/S_CleganeBowl7/',name,'.png'));
imwrite(masks(:,:,8), strcat('Data/CleganeThresh/S_CleganeBowl8/',name,'.png'));
imwrite(masks(:,:,9), strcat('Data/CleganeThresh/S_CleganeBowl9/',name,'.png'));
end
