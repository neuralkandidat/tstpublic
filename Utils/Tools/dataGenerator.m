%evaluates images with different nets and plots results

function dataGenerator(dirpath, setNumber, cam)


variables = struct;
variables.figure = figure(1);
variables.dir = dirpath;
variables.cam = cam;

if setNumber < 10
    variables.set=strcat('0',num2str(setNumber));
else
    variables.set=strcat(setNumber);
end

set(variables.figure,'KeyPressFcn', @(src,eventdata)hotkeymode1(src,eventdata,variables));

end


function takePicture(variables)
notifyPictureTaken();
variables.image = snapshot(variables.cam);

%18 AMIGOS
load('N1-Segmentation/29x29/nets/18amigos_S/net-epoch-33.mat');
subplot(3,2,1);
mask=evaluateImage(net,variables.image,0,0, true);
imagesc(mask);
title('18 AMIGOS');

variables.amigomask = mask;

%2008 IMS
load('N1-Segmentation/29x29/nets/2008ims/net-epoch-22.mat');
subplot(3,2,2);
mask=evaluateImage(net,variables.image,0,0, true);
imagesc(mask);
title('2008 ims');
variables.imsmask = mask;

%JON LIVES
load('N1-Segmentation/29x29/nets/JonLives_S/net-epoch-15.mat');
subplot(3,2,3);
mask=evaluateImage(net,variables.image,0,0, true);
imagesc(mask);
title('JON LIVES');
variables.JLmask = mask;

%JON LIVES
load('N1-Segmentation/29x29/nets/S_EaterOfWorlds/net-epoch-14.mat');
subplot(3,2,4);
mask=evaluateImage(net,variables.image,0,0, true);
imagesc(mask);
title('EaterOfWorlds');
variables.eowmask = mask;

%JON LIVES
load('N1-Segmentation/29x29/nets/S_EaterOfWorlds_dropout/net-epoch-14.mat');
subplot(3,2,5);
mask=evaluateImage(net,variables.image,0,0, true);
imagesc(mask);
title('EaterOfWorlds dropout');
variables.eowdmask = mask;


im=imresize(variables.image,[174 174]);
%maskedImage = bsxfun(@times, im, cast(mask,class(im)));

subplot(3,2,6)
imagesc(im);
title('Image');


set(variables.figure,'KeyPressFcn',{@hotkeymode2, variables});
end

function hotkeymode1(src, e,variables)

switch e.Key
    case 'space'
        takePicture(variables);
end

end

function hotkeymode2(src, e, variables)
code = char(e.Key)-0;

if strcmp(e.Key,'escape')
    dataGenerator(variables.dir, variables.set, variables.cam);
elseif (code >=97) && (code <=122)
    display(strcat('Saving as character: ', upper(char(code))));
    name = getName(variables, upper(char(code)));
    savePictures(name,variables);
end
  
end

function savePictures(name, variables)
if(exist('Data/generated/pics/')~=7)
    mkdir('Data/generated/pics')
end

imwrite(variables.image, strcat(variables.dir,'/pics/',name,'.jpg'));

if(exist('Data/generated/18Amigos/')~=7)
    mkdir('Data/generated/18Amigos')
end

if(exist('Data/generated/2008ims/')~=7)
    mkdir('Data/generated/2008ims')
end

if(exist('Data/generated/JonLives/')~=7)
    mkdir('Data/generated/JonLives')
end


if(exist('Data/generated/S_EaterOfWorlds/')~=7)
    mkdir('Data/generated/S_EaterOfWorlds')
end


if(exist('Data/generated/S_EaterOfWorlds_dropout/')~=7)
    mkdir('Data/generated/S_EaterOfWorlds_dropout')
end



imwrite(variables.amigomask, strcat(variables.dir,'/18Amigos/',name,'.png'));
imwrite(variables.imsmask, strcat(variables.dir,'/2008ims/',name,'.png'));
imwrite(variables.JLmask, strcat(variables.dir,'/JonLives/',name,'.png'));
imwrite(variables.eowmask, strcat(variables.dir,'/S_EaterOfWorlds/',name,'.png'));
imwrite(variables.eowdmask, strcat(variables.dir,'/S_EaterOfWorlds_dropout/',name,'.png'));

dataGenerator(variables.dir, variables.set, variables.cam);
end

function name = getName(variables, letter)
imList=struct2cell(dir(strcat(variables.dir,'/pics')));
imList=imList(1,:);

imListSet=strfind(imList,['S',variables.set]);
imListLetter=strfind(imList,letter);
j=0;
inds = 0;
for i=1:length(imListLetter)
    if imListLetter{i}==4
        if imListSet{i}==6
            j=j+1;
            inds(j)=i;
        end
    end
end
%
if inds == 0
    name = strcat('001',letter,'_S',variables.set);
else
    inds(inds==0)=[];
    
    numList=char(imList(inds));
    numList=numList(:,1:3);
    numList=str2num(numList);
    
    num=max(numList)+1;
    if num<10
        numstr=strcat('00',num2str(num));
    elseif num<100
        numstr=strcat('0',num2str(num));
    else
        numstr=strcat(num);
    end
    
    name=strcat(numstr,letter,'_S',variables.set);
end
end
