%evaluates images with different nets and plots results

function dataGeneratorBatch(setPath, setNumber)

if setNumber < 10
    setNumber=strcat('0',num2str(setNumber));
else
    setNumber=strcat(setNumber);
end

dir_struct = dir(strcat(setPath,'*.jpg'));

sorted_names=sortrows({dir_struct.name}');
nrOfFiles=size(sorted_names,1);

images = uint8(zeros(720,1080,3,nrOfFiles));

for i = 1:nrOfFiles
    images(:,:,:,i) = imread(sorted_names{i});
end


masks = evaluateBatch(images, nrOfFiles);

chooseAndSave(images, masks, nrOfFiles, setNumber);
end


function masks = evaluateBatch(images,nrOfFiles)
masks = zeros(174, 174, 5, nrOfFiles);

%18 AMIGOS
load('N1-Segmentation/29x29/nets/18amigos_S/net-epoch-33.mat');
for i=1:nrOfFiles
    masks(:,:,1,i) = evaluateImage(net,images(:,:,:,i),0,0, false);
end
display('Evaluated 18 Amigos')

%2008 IMS
load('N1-Segmentation/29x29/nets/2008ims/net-epoch-22.mat');
for i=1:nrOfFiles
    masks(:,:,2,i) = evaluateImage(net,images(:,:,:,i),0,0, false);
end
display('Evaluated 2008 IMS')

%JON LIVES
load('N1-Segmentation/29x29/nets/JonLives_S/net-epoch-15.mat');
for i=1:nrOfFiles
    masks(:,:,3,i) = evaluateImage(net,images(:,:,:,i),0,0, false);
end
display('Evaluated Jon Lives')

%EaterOfWorlds
load('N1-Segmentation/29x29/nets/S_EaterOfWorlds/net-epoch-14.mat');
for i=1:nrOfFiles
    masks(:,:,4,i) = evaluateImage(net,images(:,:,:,i),0,0, false);
end
display('Evaluated Eater of Worlds')

%EaterOfWorlds dropout
load('N1-Segmentation/29x29/nets/S_EaterOfWorlds_dropout/net-epoch-14.mat');
for i=1:nrOfFiles
    masks(:,:,5,i) = evaluateImage(net,images(:,:,:,i),0,0, false);
end
display('Evaluated Eater of Worlds dropout')

end

function chooseAndSave(images, masks, nrOfFiles, setNumber)

for i=1:nrOfFiles
    figure1 = figure;
    image = images(:,:,:,i);
    set(figure1,'KeyPressFcn', @(src,eventdata)hotkeys(src,eventdata,image, masks(:,:,:,i), setNumber));
    subplot(3,2,1);
    imagesc(masks(:,:,1,i));
    title('18 AMIGOS');
    
    subplot(3,2,2);
    imagesc(masks(:,:,2,i));
    title('2008 ims');
    
    subplot(3,2,3);
    imagesc(masks(:,:,3,i));
    title('JON LIVES');
    
    subplot(3,2,4);
    imagesc(masks(:,:,4,i));
    title('EaterOfWorlds');
    
    subplot(3,2,5);
    imagesc(masks(:,:,5,i));
    title('EaterOfWorlds dropout');
    
    subplot(3,2,6);
    imshow(image);
    title('Image');
    while(waitforbuttonpress==0)
        
    end
    
    close all
end


end

function hotkeys(src, e, image, masks, setNumber)
code = char(e.Key)-0;

if strcmp(e.Key,'escape')
    return;
elseif (code >=97) && (code <=122)
    display(strcat('Saving as character: ', upper(char(code))));
    name = getName(setNumber, upper(char(code)));
    savePictures(name,image, masks);
end

end

function savePictures(name, image, masks)
if(exist('Data/generated/pics/')~=7)
    mkdir('Data/generated/pics')
end

imwrite(image, strcat('Data/generated/pics/',name,'.jpg'));

if(exist('Data/generated/18Amigos/')~=7)
    mkdir('Data/generated/18Amigos')
end

if(exist('Data/generated/2008ims/')~=7)
    mkdir('Data/generated/2008ims')
end

if(exist('Data/generated/JonLives/')~=7)
    mkdir('Data/generated/JonLives')
end


if(exist('Data/generated/S_EaterOfWorlds/')~=7)
    mkdir('Data/generated/S_EaterOfWorlds')
end


if(exist('Data/generated/S_EaterOfWorlds_dropout/')~=7)
    mkdir('Data/generated/S_EaterOfWorlds_dropout')
end



imwrite(masks(:,:,1), strcat('Data/Generated/18Amigos/',name,'.png'));
imwrite(masks(:,:,2), strcat('Data/Generated/2008ims/',name,'.png'));
imwrite(masks(:,:,3), strcat('Data/Generated/JonLives/',name,'.png'));
imwrite(masks(:,:,4), strcat('Data/Generated/S_EaterOfWorlds/',name,'.png'));
imwrite(masks(:,:,5), strcat('Data/Generated/S_EaterOfWorlds_dropout/',name,'.png'));
end

function name = getName(setNumber, letter)
imList=struct2cell(dir(strcat('Data/Generated/pics')));
imList=imList(1,:);

imListSet=strfind(imList,['S',setNumber]);
imListLetter=strfind(imList,letter);
j=0;
inds = 0;
for i=1:length(imListLetter)
    if imListLetter{i}==4
        if imListSet{i}==6
            j=j+1;
            inds(j)=i;
        end
    end
end
%
if inds == 0
    name = strcat('001',letter,'_S',setNumber);
else
    inds(inds==0)=[];
    
    numList=char(imList(inds));
    numList=numList(:,1:3);
    numList=str2num(numList);
    
    num=max(numList)+1;
    if num<10
        numstr=strcat('00',num2str(num));
    elseif num<100
        numstr=strcat('0',num2str(num));
    else
        numstr=strcat(num);
    end
    
    name=strcat(numstr,letter,'_S',setNumber);
end
end
