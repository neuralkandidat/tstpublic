%Calculate Jaccard index for masks in "path".
%path should have one folder called "gt" and at least
%one folder with masks from a segmentation net

%masks should end with Sxx.png where xx is the set number

%path should NOT end with a "/"

%Returns: matrix with jaccard indexes
%         each row in the matrix represents one neural network

function J = calculateJaccardIndex(path,set)

%get directory names for which we want to loop through
%ignore ., .., ds_store, pics and gt
dirs=dir(path);
dirNames = {dirs([dirs.isdir]).name};
dirNames = dirNames(~ismember(dirNames,{'.','..','pics','gt'}))

%set should be a string with two digits
if set<10
    set=strcat('0',num2str(set));
else
    set=num2str(set);
end

gt_path=strcat(path,'/gt/');
gt_dir=strcat(path,'/gt/*',num2str(set),'.png');
gt_struct=dir(gt_dir);

J=zeros(length(dirNames), length(gt_struct));

%go through all masks in gt_struct
for i=1:length(gt_struct)
    
    gt=imread(strcat(gt_path, gt_struct(i).name));
    
    %go through all evaluated nets
    for j=1:length(dirNames)
        
        im_path=strcat(path,'/',dirNames{j},'/',gt_struct(i).name);
        try
            im=imread(im_path);
            im=imresize(im,size(gt));
            J(j,i)=bBoxOverlap(gt,im);
        catch
            J(j,i)=0;%????
        end
        
        
    end
    
end




