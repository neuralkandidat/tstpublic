%evaluates images with different nets and plots results

function compareNetVisual(im_path)
im=imread(im_path);

figure

%18 AMIGOS
load('N1-Segmentation/29x29/nets/18amigos_S/net-epoch-33.mat')
subplot(3,2,1)
mask=evaluateImage(net,im_path,0,0,1);
imagesc(mask);
title('18 AMIGOS')

%2008 IMS
load('N1-Segmentation/29x29/nets/2008ims/net-epoch-22.mat')
subplot(3,2,2)
mask=evaluateImage(net,im_path,0,0,1);
imagesc(mask);
title('2008 ims')

%JON LIVES
load('N1-Segmentation/29x29/nets/JonLives_S/net-epoch-15.mat')
subplot(3,2,3)
mask=evaluateImage(net,im_path,0,0,1);
imagesc(mask);
title('JON LIVES')


%EaterOfWorlds_dropout
load('N1-Segmentation/29x29/nets/S_EaterOfWorlds_dropout//net-epoch-14.mat')
subplot(3,2,4)
mask=evaluateImage(net,im_path,0,0,1);
imagesc(mask);
title('EaterOfWorlds w dropout')


%EATER OF WORLDS
load('N1-Segmentation/29x29/nets/S_EaterOfWorlds/net-epoch-14.mat')
subplot(3,2,5)
mask=evaluateImage(net,im_path,0,0,1);
imagesc(mask);
title('EATER OF WORLDS')



im=imresize(im,[174 174]);
maskedImage = bsxfun(@times, im, cast(mask,class(im)));


subplot(3,2,6)
imagesc(im);
title('Image');


