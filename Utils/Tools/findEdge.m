function edge=findEdge(M)

x=find(sum(M(:,:,1)));
y=find(sum(M(:,:,1)'));

%x_1,x_2,y_1,y_2
edge=[x(1),x(end),y(1),y(end)];
end