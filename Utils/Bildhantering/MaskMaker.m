function varargout = MaskMaker(varargin)
% MASKMAKER MATLAB code for MaskMaker.fig
%      MASKMAKER, by itself, creates a new MASKMAKER or raises the existing
%      singleton*.
%
%      H = MASKMAKER returns the handle to a new MASKMAKER or the handle to
%      the existing singleton*.
%
%      MASKMAKER('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MASKMAKER.M with the given input arguments.
%
%      MASKMAKER('Property','Value',...) creates a new MASKMAKER or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before MaskMaker_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to MaskMaker_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help MaskMaker

% Last Modified by GUIDE v2.5 15-Feb-2016 23:01:13

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @MaskMaker_OpeningFcn, ...
    'gui_OutputFcn',  @MaskMaker_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before MaskMaker is made visible.
function MaskMaker_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to MaskMaker (see VARARGIN)

%Slider stuff
numSteps = 10;
set(handles.RadiusSlider, 'SliderStep', [1/(numSteps-1) , 1/(numSteps-1) ]);
set(handles.RadiusSlider, 'Min', 1);
set(handles.RadiusSlider, 'Max', numSteps);
set(handles.RadiusSlider, 'Value', 3);
set(handles.RadiusSlider, 'SliderStep', [1/(numSteps-1) , 1/(numSteps-1) ]);
handles.lastSliderVal = get(handles.RadiusSlider,'Value');

%handles = load_image('005H-01-A-D.jpg', handles)
handles.drawmode='draw';
handles.currenttype='RGB';
handles.maskcolor='black';

%Hotkeys
set(handles.figure1,'ButtonDownFcn',@start_pencil);
set(handles.figure1,'KeyPressFcn',@hotkey);

% Choose default command line output for MaskMaker
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
%repaintimage(handles);


% UIWAIT makes MaskMaker wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = MaskMaker_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
guidata(hObject, handles);

% --- Executes on button press in switchbutton.
function switchbutton_Callback(hObject, eventdata, handles)
% hObject    handle to switchbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
switch(handles.currenttype)
    case('RGB')
        handles.currenttype='mask';
    case('mask')
        handles.currenttype='RGB';
end
guidata(hObject,handles);
repaintimage(handles);

% --- Executes on button press in unzoombutton.
function unzoombutton_Callback(hObject, eventdata, handles)
% hObject    handle to unzoombutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles=guidata(hObject);
handles.zoom='auto';
guidata(hObject,handles);

axis(handles.zoom);

% --- Executes on selection change in imagelistbox.
function imagelistbox_Callback(hObject, eventdata, handles)
% hObject    handle to imagelistbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%if ~strcmp(get(gcf,'SelectionType'),'open')
%
%    index_selected = get(handles.imagelistbox,'Value');
%    list = get(handles.imagelistbox,'String');
%    item_selected = list{index_selected};
%
%    switch item_selected
%        case '.'
%
%        case '..'
%
%        otherwise
%            [pathstr,name,ext] = fileparts(filename)
%    end
%end

%handles = load_image(item_selected, handles);


% --- Executes during object creation, after setting all properties.
function imagelistbox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to imagelistbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

load_listbox(pwd, hObject);
guidata(hObject,handles);
guidata(gca,handles);


% --- Executes on button press in savebutton.
function savebutton_Callback(hObject, eventdata, handles)
% hObject    handle to savebutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%dlmwrite(strcat(handles.currentpath,'_mask'), handles.maskimage);
imwrite(handles.maskimage,strcat(handles.currentpath,'_mask.png'));
%load_listbox(pwd, handles.imagelistbox);

% --- Executes on button press in loadbutton.
function loadbutton_Callback(hObject, eventdata, handles)
% hObject    handle to loadbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%handles=guidata(gca);
index_selected = get(handles.imagelistbox,'Value');
list = get(handles.imagelistbox,'String');
item_selected = list{index_selected}; % Convert from cell arr
handles = load_image(item_selected, handles);
repaintimage(handles);
guidata(gca, handles);

% --- Executes on slider movement.
function RadiusSlider_Callback(hObject, eventdata, handles)
% hObject    handle to RadiusSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

newVal = floor(get(hObject,'Value'));
% set the slider value to this integer which will be in the set {1,2,3,...,12,13}
set(hObject,'Value',newVal);

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
if newVal ~= handles.lastSliderVal
    % it is different, so we have moved up or down from the previous integer
    % save the new value
    handles.lastSliderVal = newVal;
end
guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function RadiusSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to RadiusSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in zoombutton.
function zoombutton_Callback(hObject, eventdata, handles)
% hObject    handle to zoombutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(gca,'HitTest','on');
set(allchild(gca),'HitTest','on');
h = imrect;
pos=h.getPosition;
h.delete;

handles.zoom = [pos(1) pos(1)+pos(3) pos(2) pos(2)+pos(4)];

guidata(hObject,handles);
repaintimage(handles);

function start_pencil(src,eventdata)
coords=get(gca,'currentpoint'); %this updates every time i move the mouse

x=round(coords(1,1,1));
y=round(coords(1,2,1));

drawpixels(x,y,src);

set(src,'WindowButtonMotionFcn',@continue_pencil);
set(src,'WindowButtonUpFcn',@done_pencil);

function continue_pencil(src,eventdata)
%handles=guidata(src)
%Note: src is now the figure handle, not the axes, so we need to use gca.
coords=get(gca,'currentpoint'); %this updates every time i move the mouse

x=round(coords(1,1,1));
y=round(coords(1,2,1));

drawpixels(x,y,src);

function done_pencil(src,eventdata)
handles=guidata(src);
set(src,'windowbuttonmotionfcn','');
set(src,'windowbuttonupfcn','');
repaintimage(handles);

function drawpixels(x,y,src)
handles=guidata(src);
r=get(handles.RadiusSlider,'Value');

for i=x-r:x+r
    for j=y-r:y+r
        if (floor((i-x)^2+(j-y)^2+2)<r^2)||(x==i && y==j)
            switch (handles.drawmode)
                case ('draw')
                    handles.RGBimage(j,i,:)=handles.originalimage(j,i,:);
                    handles.maskimage(j,i,:)=1;
                    switch(handles.currenttype)
                        case('RGB')
                            rectangle('Position',[(i-0.5) (j-0.5) 1 1],...
                                'FaceColor',reshape(handles.RGBimage(j,i,:),1,3),...
                                'EdgeColor','none');
                        case('mask')
                            rectangle('Position',[(i-0.5) (j-0.5) 1 1],...
                                'FaceColor','white',...
                                'EdgeColor','none');
                    end
                    
                case ('erase')
                    
                    handles.RGBimage(j,i,:)=0;
                    handles.maskimage(j,i,:)=0;
                    rectangle('Position',[(i-0.5) (j-0.5) 1 1],...
                        'FaceColor',handles.maskcolor,...
                        'EdgeColor','none');
            end
        end
    end
end
guidata(src,handles);

function repaintimage(handles)

switch (handles.currenttype)
    case ('RGB')
        m=handles.RGBimage;
        n=handles.maskimage;
        
        switch handles.maskcolor
            case 'black'
                m(n==0)=0;
            case 'green'
                m(n==0)=0;
                subm=m(:,:,2);
                subm(n==0)=255;
                m(:,:,2)=subm;
            case 'blue'
                m(n==0)=0;
                subm=m(:,:,3);
                subm(n==0)=255;
                m(:,:,3)=subm;
        end
        handles.RGBimage=m;
        imshow(m);
        
    case ('mask')
        imshow(handles.maskimage);
end

axis(handles.zoom);
set(gca,'HitTest','off');
set(allchild(gca),'HitTest','off');
guidata(gca,handles);

% --- Executes on button press in radiobutton8.
function radiobutton8_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton8
button_state = get(hObject,'Value');
if button_state == get(hObject,'Max')
    handles.drawmode='draw';
end
guidata(hObject,handles);

% --- Executes on button press in radiobutton9.
function radiobutton9_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of radiobutton9
button_state = get(hObject,'Value');
if button_state == get(hObject,'Max')
    handles.drawmode='erase';
end
guidata(hObject,handles);

function hotkey(src, e)
handles = guidata(src);
[num, numerical] = str2num(e.Key);
if numerical
    set(handles.RadiusSlider, 'Value', num);
else
    switch e.Key
        case 'q'
            switch handles.drawmode
                case 'erase'
                    set(handles.radiobutton8,'Value',1)
                    handles.drawmode='draw';
                case 'draw'
                    set(handles.radiobutton9,'Value',1)
                    handles.drawmode='erase'
            end
            guidata(src,handles);
        case 'w'
            unzoombutton_Callback(src,[], handles);
        case 'e'
            zoombutton_Callback(src,[], handles);
        case 'x'
            savebutton_Callback(src,[],handles);
            index_selected = get(handles.imagelistbox,'Value');
            list = get(handles.imagelistbox,'String');
            item_selected = list{index_selected+1}; % Convert from cell arr
            set(handles.imagelistbox,'Value', index_selected+2);
            handles = load_image(item_selected, handles);
            repaintimage(handles);
            guidata(gca, handles);
    end
end

function load_listbox(dir_path, hObject)
dir_struct = dir(dir_path);
[sorted_names,sorted_index] = sortrows({dir_struct.name}');

handles.file_names = sorted_names;
handles.sorted_index = sorted_index;

guidata(gca,handles);
set(hObject,'String',handles.file_names,...
    'Value',1)

function handles=load_image(image_path, handles)
%Image stuff
[cdata,map] = imread(image_path);
handles.originalimage = cdata;
handles.current_data = cdata;
handles.drawmode='draw';

handles.currenttype='RGB';
handles.currentpath=image_path;

imshow(handles.current_data);

h = imrect;
pos=h.getPosition;
handles.zoom = [pos(1) pos(1)+pos(3) pos(2) pos(2)+pos(4)]

axis(floor(handles.zoom));
h.delete;


h = imfreehand;
M=h.createMask;
h.delete;
handles.maskimage=M;

M3=repmat(M,1,1,3);

handles.RGBimage=handles.originalimage;
handles.RGBimage(~M3)=0;

axis(floor(handles.zoom));


% --- Executes on button press in blackradio.
function blackradio_Callback(hObject, eventdata, handles)
% hObject    handle to blackradio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

button_state = get(hObject,'Value');
if button_state == get(hObject,'Max')
    handles.maskcolor='black';
    repaintimage(handles);
end
guidata(hObject,handles);

% --- Executes on button press in greenradio.
function greenradio_Callback(hObject, eventdata, handles)
% hObject    handle to greenradio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

button_state = get(hObject,'Value');
if button_state == get(hObject,'Max')
    handles.maskcolor='green';
    repaintimage(handles);
end
guidata(hObject,handles);

% --- Executes on button press in blueradio.
function blueradio_Callback(hObject, eventdata, handles)
% hObject    handle to blueradio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

button_state = get(hObject,'Value');
if button_state == get(hObject,'Max')
    handles.maskcolor='blue';
    repaintimage(handles);
end
guidata(hObject,handles);
