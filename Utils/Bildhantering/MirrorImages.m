function MirrorImages(path)

dir_struct = dir(strcat(path,'/*.jpg'));

sorted_names=sortrows({dir_struct.name}');
nrOfFiles=size(sorted_names,1);

for i = 1:nrOfFiles
    filename = sorted_names{i}(1:11);
    
    
    %Reverse only specified images. Persons 08 and 10 are left handed 
    if strcmp(filename(6:7),'08')||strcmp(filename(6:7),'10')
        ReverseImage(path,filename);
    end
end

end

function ReverseImage(path,file)
imagepath = strcat(path,'/',file,'.jpg');
maskpath = strcat(path,'/',file,'_mask.png');
%Get paths for image and mask

image = imread(imagepath);
mask = imread(maskpath);
%Read image and mask

if isa(mask, 'uint8')
    mask=logical(mask);
end
%images created by gimp were read as uint8, which didnt work with imwrite

image=fliplr(image);
mask=fliplr(mask);
%flip matrices

imwrite(image,imagepath)
imwrite(mask,maskpath)
%save reversed images
end