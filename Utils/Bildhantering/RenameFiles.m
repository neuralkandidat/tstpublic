function RenameFiles(path)
dir_struct = dir(strcat(path,'/*.png'));
%Get file information for all mask images

sorted_names=sortrows({dir_struct.name}');
nrOfFiles=size(sorted_names,1);


for i = 1:nrOfFiles
    filename = sorted_names{i};
    newname=strcat(filename(1:11),'_mask.png');
    %create new names without .jpg in mask name
    
    movefile(strcat(path,'/',filename),...
        strcat(path,'/',newname));
end