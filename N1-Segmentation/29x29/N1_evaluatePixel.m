function probability = N1_evaluatePixel(net, pixelArea)
%evaluates one pixel using its surrounding area.
res = vl_simplenn(net, pixelArea,[],[],'mode','test');
probability = res(end).x(:,:,1);