%randomly crops away areas without hand, keeps aspect ratio
function [image, gt]=N1_randomizeImage(image,gt)


%find coordinates for edges of hand [x_1,x_2,y_1,y_2]
edges=findEdge(gt);

[H W]=size(gt);

xLeft=random('unid',edges(1));
xRight=edges(2)+random('unid',max(W-edges(2),1));
yTop=random('unid',edges(3));
yBottom=edges(4)+random('unid',max(H-edges(4),1));


prop1 = min(xLeft/W,yTop/H);
prop2 = min(1-xRight/W,1-yBottom/H);
if prop2<0
    prop2=0;
end

xStart=max(floor(W*prop1),1);
yStart=max(floor(H*prop1),1);
xStop=ceil(W*(1-prop2)); 
yStop=ceil(H*(1-prop2));

gt=gt(yStart:yStop,xStart:xStop);
image=image(yStart:yStop,xStart:xStop,:);
