function output = N1_evaluateImage(net, imagepath)

    %Default values. Should be passed as parameter varargin
    size = 174;
    padding = 14;
    gpu = false;
    cspace='luv';

    if (gpu==true)
        net = vl_simplenn_move(net, 'gpu');
    end
    %Change last layer of net to allow evaluation
    net.layers{end} = [];
    net.layers{end}.type = 'softmax';

    %read image
    image=imread(imagepath);

    image=N1_preprocessImage(image, size, padding, cspace);

    output = zeros(size,size);
    %case outside loop to limit amount of if-statements, may be improved?
    switch gpu
        case true
            for i=1:size
                for j=1:size
                    output(i,j)=N1_evaluatePixel(net, ...
                        image(i:i+2*padding,j:j+2*padding,:));
                end
            end
        case false
            for i=1:size
                for j=1:size
                    output(i,j)=N1_evaluatePixel(net, ...
                        image(i:i+2*padding,j:j+2*padding,:));
                end
            end
    end
end