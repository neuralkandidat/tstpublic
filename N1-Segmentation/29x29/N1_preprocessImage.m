function image = N1_preprocessImage(image, size, padding, ctransform)
%resize image
image = imresize(image, [size size]);

%change colorspace of image
image = colorspace(strcat('rgb->',ctransform),image);

image=normalize(image);

%pad image to allow evaluation of corner/edge pixels
image = padarray(image,[padding padding]);
%change class to single. Required for net evaluation input
image = im2single(image);

end

function im_mean=calculateImageMean(image);
    im_mean=mean(mean(image(:,:)));
end 

function im_std=calculateImageStd(image);
        im_std=std(std(image(:,:)));
end

function image=normalize(image);
    for i=1:3
        image(:,:,i)=(image(:,:,i)-calculateImageMean(image(:,:,i)))./calculateImageStd(image(:,:,i));
    end
end
