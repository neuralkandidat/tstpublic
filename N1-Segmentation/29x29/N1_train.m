function N1_train(name,path,numEpochs,randomize,fixProp)

for epoch=1:numEpochs
    if exist(strcat('N1-Segmentation/29x29/nets/',name))~=7
        clear net;
        net=N1_initCNN();
    else
        temp=dir(strcat('N1-Segmentation/29x29/nets/',name));
        if length(temp) > 3
            load(strcat('N1-Segmentation/29x29/nets/',name,'/',temp(end-1).name));
        else
            load(strcat('N1-Segmentation/29x29/nets/',name,'/',temp(end).name));
        end
    end
    
    imdb=N1_makeImdb(path,name,randomize,fixprop);
    net = initTrain(imdb, epoch, name, net);
    
end

end

function net=initTrain(imdb, numEpochs, expDir, net)
trainOpts.batchSize = 256;
trainOpts.numEpochs = numEpochs ;
trainOpts.continue = true ;
trainOpts.gpus = [1]; % set to 1 to use GPU for training, otherwise blank
trainOpts.cudnn = true;
trainOpts.learningRate = 0.001 ;
trainOpts.weightDecay = 0.05 ;
trainOpts.profile = false;
trainOpts.prefetch = false;
opts.train.errorFunction = 'label';
trainOpts.expDir = strcat('N1-Segmentation/29x29/nets/',expDir) ;
trainOpts = vl_argparse(trainOpts, {});
[net,info] = cnn_train(net, imdb, @N1_getBatch_v2, trainOpts) ;
end
