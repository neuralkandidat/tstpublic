function [im,label]=N1_getBatch_v2(imdb,batch)
    p = 14;                 % padding
    w = 174;                % width of original pictures and ground truths
    n = ceil(batch/w^2);    % #image
    r = batch-(n-1)*w^2;    % the index of the pixel disregarding
                            % previous amount of images
    row = floor((r-1)/w);   % #row for the pixel

    index = (n-1).*(2*p+w).^2+2*p^2+w*p+2*p*row+p+r; 
    %computes the pixel index for a matrix with padding
    % (n-1).*(2*p+w).^2 <- Disregard previous images
    % 2*p^2  <- Top left and top right corners of a padded matrix
    % w*p    <- Top center part of a padded matrix
    % 2*p*row <- Amount of rows of padding

    [I,J,L]=ind2sub(size(imdb.images.data),index);

    im=single(ones(29,29,3,length(batch)));
    label=ones(1,1,length(batch));

    [X,Y,N] = ind2sub(size(imdb.images.gt),batch);

    for k = 1:length(batch)
        im(:,:,:,k)=imdb.images.data(I(k)-p:I(k)+p,J(k)-p:J(k)+p,:,L(k));
        label(k) = imdb.images.gt(X(k),Y(k),N(k));
    end
    label=squeeze(label);
end