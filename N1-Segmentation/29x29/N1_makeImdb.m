%Function creates an IMDB structure


%Preconditions:
%
%Directories:   There must be a parent directory which includes the
%               subfolders 'masks' and 'pics'. The structure of the two
%               subfolders does not matter as long as they are identical.
%
%Image files:   Files for masks must have identical name to the image file,
%               with the addition of '_mask',
%               e.g '001I-01-A-C.jpg' and '001I-01-A-C_mask.png'
%
%Parameters:    parentDir is a string containing the parent directory of
%               'masks' and 'pics' folders, e.g 'Data'.
%
%               name is a string containing the name that the imdb should
%               be saved as, e.g 'imdb_1000ims_1'.
%
%               randomize is a logical,
%               1 to enable randomization of images, 0 to disable

function imdb=N1_makeImdb(parentDir,name,randomize,propFix);
imdb={};
imdb.images.data={};
imdb.images.set=[];
imdb.images.id=[];
imdb.images.gt=[];
imdb.meta.mean={};
imdb.meta.sets={'train','val','test'};

% Check OS, adjust accordingly
if ispc
    separator = '[^;]*';
else
    separator='[^:]*';
end

% Store the paths to the subfolders as a 1xN cell,
% N is number of subfolders.
%    maskdirs = regexp(genpath(strcat(parentDir,'/masks')),[separator],'match');
%    imdirs = regexp(genpath(strcat(parentDir,'/pics')),[separator],'match');

maskdirs = dir(strcat(parentDir,'/masks'));
isub = [maskdirs(:).isdir]; %# returns logical vector
maskdirs = {maskdirs(isub).name}';
maskdirs(ismember(maskdirs,{'.','..'})) = [];

imdirs = dir(strcat(parentDir,'/pics'));
isub = [imdirs(:).isdir]; %# returns logical vector
imdirs = {imdirs(isub).name}';
imdirs(ismember(imdirs,{'.','..'})) = [];

for i=1:length(maskdirs) %omits i=1 since this refers to parentDir itself
    maskdir=char(strcat(parentDir,'/masks/',maskdirs(i)));
    imdir=char(strcat(parentDir,'/pics/',imdirs(i)));
    maskpaths=dir(maskdir);
    impaths=dir(imdir);
    
    % Remove elements of paths that start with '.', e.g hidden files.
    maskpaths=removeIllegal(maskpaths);
    impaths=removeIllegal(impaths);
    
    for j=1:length(impaths)
        % If the mask name does not match the image name, an error will
        % be thrown. Name of current image and mask included in message
        if checkMatch(impaths(j).name,maskpaths(j).name)
            imdb=addToImdb(strcat(imdir,'/',impaths(j).name),strcat(maskdir,'/',maskpaths(j).name),imdb,randomize,propFix);
        else
            error(strcat('Image does not match mask. Image: ',imdir,'/', impaths(j).name,', mask: ',maskdir,'/',maskpaths(j).name));
        end
    end
    
    imdb.images.set(length(imdb.images.set)+1:length(imdb.images.set)+length(impaths)*174*174)=1;
    imdb.images.set(length(imdb.images.set)-round(0.2*length(impaths)*174*174):length(imdb.images.set))=2;
    
end

imdb.images.data=cellTo4DMatrix(imdb.images.data);
imdb.images.gt=cellTo3DMatrix(imdb.images.gt);
imdb=padImdb(imdb);

% Hand = 1, no hand = 2
imdb.images.gt=imdb.images.gt+1;

% Create a imdb.images.set, parameters are the amounts
% (0 to 1) of elements in train, val and test. Random order.
%imdb=createSet(0.8,0.2,0,imdb);
imdb.images.id=1:numel(imdb.images.gt);

% Create folder imdbs if it does not exist
if exist('Data/imdbs')~=7
    mkdir('Data/imdbs')
end
save(strcat('Data/imdbs/', name),'imdb','-v7.3')
end

function imdb=addToImdb(imagepath,maskpath,imdb,randomize, propFix);
im_size=174;

image = im2double(imread(imagepath));
mask = logical(imread(maskpath));

image = colorspace('rgb->luv',image);
image = normalize(image);

if propFix
    [image,mask]=fixProp(image,mask);
end

if randomize==1
    [image,mask] = N1_randomizeImage(image,mask);
end

mask = imresize(mask,[im_size im_size]);
image = imresize(image,[im_size im_size]);
image = single(image);

% Thresholding
mask(mask<0.5) = 0;
mask(mask>=0.5) = 1;

% Switching so hand = 0, no hand = 1
mask = (mask - 1)*(-1);

imdb.images.data{end+1} = image;
imdb.images.gt{end+1} = mask;
end

function im_mean=calculateImageMean(image);
im_mean=mean(mean(image(:,:)));
end

function im_std=calculateImageStd(image);
im_std=std(std(image(:,:)));
end

function image=normalize(image);
for i=1:3
    image(:,:,i)=(image(:,:,i)-calculateImageMean(image(:,:,i)))./calculateImageStd(image(:,:,i));
end
end

function imdb=padImdb(imdb);
imdb.images.data=padarray(imdb.images.data,[14 14],0);
%    imdb.images.gt=padarray(imdb.images.gt,[14 14],1);
end

function imdb=createSet(trainAmount,valAmount,testAmount,imdb);
train=ones(1,round(trainAmount*numel(imdb.images.gt)));
val=2*ones(1,round(valAmount*numel(imdb.images.gt)));
test=3*ones(1,round(testAmount*numel(imdb.images.gt)));

imdb.images.set=horzcat(train,val,test);
imdb.images.set(randperm(length(imdb.images.set)));
end


function M=cellTo4DMatrix(data)
[~, N] = size(data);
[W, H, C] = size(data{1});
M=zeros(W,H,C,N);

for i=1:N
    M(:,:,:,i)=data{i};
end

end

function M=cellTo3DMatrix(data)
[~, N] = size(data);
[W, H, C] = size(data{1});
M=zeros(W,H,N);

for i=1:N
    M(:,:,i)=data{i};
end
end

function A=removeIllegal(B)
A=B;
for i=1:length(B)
    if B(length(B)-i+1).name(1) == '.'
        A(length(B)-i+1)=[];
    end
end
end

function match=checkMatch(image,mask);
image=strsplit(image,'.');
mask=strsplit(mask,'.');

if (strcat(image{1},'_mask') == mask{1})
    match=1;
else
    match=0;
end
end

function [image,gt]=fixProp(image,gt);

width = size(image,2);
height = size(image,1);
edges = findEdge((gt-2)*(-1));

xStart=edges(1);
yStart=edges(3);
xStop=edges(2);
yStop=edges(4);
xMid=round((xStart+xStop)/2);
yMid=round((yStart+yStop)/2);

if ((height>width) && ((yStop-yStart)<width*(9/16))) %hand fits inside new height
    outWidth=width;
    outHeight=round(width*(9/16));
    if ((yMid-round(outHeight/2)) < 0)
        imTop = 0;
        imBottom = outHeight;
    elseif ((yMid+round(outHeight/2)) > height)
        imTop = height-outHeight;
        imBottom = height;
    else
        imTop = yMid-round(outHeight/2);
        imBottom = yMid+round(outHeight/2);
    end
    image = image(imTop:imBottom,:,:);
    gt = gt(imTop:imBottom,:);
end

if ((height>width) && ((yStop-yStart)>width*(9/16))) %hand doesnt fit inside new height : pad the image
    outHeight = yStop-yStart;
    imTop = yStart;
    imBottom = yStop;
    outWidth = outHeight*(16/9);
    image = padarray(image, [1, round((outWidth-width)/2)], 'both');
    gt = padarray(gt, [1, round((outWidth-width)/2)], 2, 'both');
    image = image(imTop:imBottom,:,:);
    gt = gt(imTop:imBottom,:);
end

end
